console.log ('hello world');
console.error ('this is an error');
console.warn ('this is a warning');

/*var (not used anymore due to general aspect of duplicity) 
let, const (to be used instead - have a block-level scope)
with let we can re-assign values, with const no - it's a constant
use const unless you know you will re-assign it*/
 
let age = 30;
age = 31;
console.log (age);


//data-types: strings, numbers, boolean, null, undefined, symbol

const name = 'John';
const age1 = 30;
const rating = 4.5;
const isCool = true;
const x = null;
const y = undefined;
let z;
console.log (typeof rating);

//concatenation

console.log ('My name is ' + name + ' and I am ' + age1 );

//template string (to be used backticks ` instead of single quotes ')

console.log (`My name is ${name} and I am ${age1}`);
const hello = `My name is ${name} and I am ${age1}` ;
console.log (hello);

//string properties and methods (methods are functions associated with objects)

const s = 'Hello World';
console.log (s.length);
console.log (s.toUpperCase());
console.log (s.toLowerCase());
console.log (s.substring(0, 5).toUpperCase());
console.log (s.split(''));
console.log (s.split('e'));

const s1 = 'Technology, Computers, Code';
console.log (s1.split(', '));

//arrays - var that hold multiple values

const fruits = ['apples', 'oranges', 'pears', '10', 'true' ];
console.log(fruits[2]);
fruits[5] = 'grapes';
console.log(fruits);
fruits.push('mangos');
console.log(fruits);
fruits.unshift('strawberries');
console.log(fruits);
fruits.pop();
console.log(fruits);
console.log(Array.isArray('hello'));
console.log(fruits.indexOf('oranges'));

//object literals

const person = {
    firstname: 'John',
    lastname: 'Doe',
    age: 30,
    hobbies: ['music', 'movies', 'sports'],
    address: {
        street: '50 main street',
        city: 'Valletta',
        state: 'Msida',
    }
}
console.log(person); 
console.log(person.firstname, person.lastname); 
console.log(person.hobbies[1]);
console.log(person.address.city); 

        //pull out values = create variables
        const {firstname, lastname, address: {city }} = person;
        console.log(firstname);
        console.log(city);

        //adding properties
        person.email ='john@gmail.com';
        console.log(person.email);

//arrays of objects
const todos = [
    {
    id: 1,
    text: 'Pay subscription',
    isCompleted: true
    },
    {
    id: 2,
    text: 'Meeting with Alex',
    isCompleted: true
    },
    {
    id: 3,
    text: 'Haircut appt',
    isCompleted: false 
    },
]
console.log(todos);
console.log(todos[1].text);

/* JSON is a data format
    - used in Full WebStack Dev using API's when we are sending/receiving
     data to a server;
    - it's very similar to Object Literals (above);
    - www.freeformatter.com/json-formatter.html;
    - or convert to JSON in script as below:
*/
   const todoJSON = JSON.stringify(todos);
   console.log(todoJSON);

// 'For' loops
   for (let i = 0; i <= 10; i++) {
    console.log(`For loop number: ${i}`);
   }

// 'While' loops
   let i = 0;
   while (i <= 10) {
       console.log(`While loop number: ${i}`);
       i++;
   }

   // loops through an array
   for (i = 0; i < todos.length; i++) {
       console.log(todos[i].text);
   }
   for (let todo of todos) {
       console.log(todo.text);
       console.log(todo.id);
   }

        /* high order array methods:
        - forEach :loops through them;
        - map :create a new array from an array;
        - filter :create a new array based on a condition
        */

        todos.forEach(
            function(todo) {
                console.log(todo.text);
            }
            );
        
        const todoText = todos.map(
            function(todo) {
                return todo.text;
            });
            console.log(todoText);

        const todoCompleted = todos.filter(
            function(todo) {
                return todo.isCompleted === true;
            })
            console.log(todoCompleted);

        // chain on other array methods

        const todoCompleted1 = todos.filter(
            function(todo) {
                return todo.isCompleted === true;
            }).map(function(todo) {
                    return todo.text;
                    //return only the text of true
                }) 
            console.log(todoCompleted1);

// Conditionals
    const q = 4;
    const t = 11;

    if (q == 10) {
        /* == dont consider data types but === does
         const q = '10'; (string)
         if (q == 10)...(string and number - ok)
         if (q === 10)...(Nok) */

        console.log('q is 10');
    } else if(q > 10) {
        console.log('q is greater than 10')
    }
    
    else {
        console.log('q is less than 10') 
    }

    if (q > 5 || t > 10) {
        console.log('q is more than 5 or t is more than 10')
    }
    if (q > 5 && t > 10) {
        console.log('q is more than 5 or t is more than 10')
    }

/* conditional ternary operator - assigns a value to a variable
    based on some condition + switch*/

    const j = 11;
    const color = j > 10 ? 'red' : 'blue';
    //const color = 'green'; will get 'color is NOT red or blue'

    console.log(color);

    switch (color) {
        case 'red':
            console.log('color is red');
            break;
        case 'blue':
            console.log('color is blue');
            break;
        default:
            console.log('color is NOT red or blue');
            break;
    }

// functions

    function addNums(num1, num2) {
    console.log(num1+num2);
    }
    addNums(5, 4);

    function addNums1(num1, num2) {
    console.log(num1+num2);
    }
    //NaN = Not a Number
    addNums1();

    function addNums2(num1 = 1, num2 = 2) {
    console.log(num1+num2);
    }
    // set default values to our parameters
    addNums2();

    function addNums2(num1 = 1, num2 = 2) {
    console.log(num1+num2);
    }
    // overwrite default param.
    addNums2(5, 5);

    //usually functions 'return' something
    function addNums3(num1 = 1, num2 = 2) {
        return num1 + num2;
    }
    console.log(addNums3(5, 7));

//arrow functions = function as a variable 'const'
    
    const addNums4 = (num1, num2) => {
    return num1 + num2;
    }
    console.log(addNums4(5, 8));

    //or to simplify
    const addNums5 = (num1, num2) => console.log(num1+num2)
        addNums5(5, 9);

    //to return is no need even to use 'return'
    const addNums6 = (num1, num2) => {return num1+num2}
        console.log(addNums6(5, 10));
    
    const addNums7 = (num1, num2) => num1+num2;
        console.log(addNums7(5, 11));

    //if we have only 1 parameter - no ()
    const addNums8 = num1 => num1 + 5;
        console.log(addNums8(12));
 
/* OOP - can do in 2 ways:
            - constructive functions with prototypes (function
                name will always start with capital letter "P-Person")
            - ES6 classes */
   
    //Constructive functions + prototypes

    function Person (firstname, lastname, dob) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.dob = new Date(dob);
            Person.prototype.getBirthYear = function () {
                return this.dob.getFullYear();
                }
            Person.prototype.getfullname = function () {
                return `${this.firstname} ${this.lastname}`;
                }
        //Above instantiate object
        }
        const person1 = new Person('John', 'Doe', '4-3-1980');
        const person2 = new Person('Mary', 'Johnson', '8-3-1970');

        console.log(person2);
        console.log(person1.dob);
        console.log(person1.getBirthYear());
        console.log(person2.getfullname());

    /*Classes = does same thing as constructions/proto but
                syntax is easier to write */

    class Personn {
        constructor(firstnname, lastnname, dobb) {
            this.firstnname = firstnname;
            this.lastnname = lastnname;
            this.dobb = new Date(dobb);
            }
        getbirthyyear() {
            return this.dobb.getFullYear();
            }
        getfullnname() {
            return `${this.firstnname} ${this.lastnname}`
            }
        }

        const personn1 = new Personn('John', 'Doe', '4-3-1980');
        const personn2 = new Personn('Mary', 'Johnson', '8-3-1970');

        console.log(personn2);
        console.log(personn1.dobb);
        console.log(personn1.getbirthyyear());
        console.log(personn2.getfullnname());

/* DOM
 - Selectors - selecting things from DOM
        - single elem. selectors
        - multiplle elem. selectors */

    console.log(window);
    /*window - in the browser we have the
                window object -the parent
        From the window document we want to
        select objects - DOM
        alert();
        window.alert();*/

    //Single elem
    const form = document.getElementById('my-form');
    console.log(form);
    /*Query selector 
        JQuery - JS library make easy to select other things
                that ID's like classes, tags, etc...*/
    console.log(document.querySelector('my-form'));
    console.log(document.querySelector('.container'));
        /* it's a single selector so even if we select h1
            it will select only the first h1 */
    console.log(document.querySelector('h1'));

    //Multiple elem 
        /*for classes, tags to be used the newest querysel.
        and not the oldest getElem.*/
    console.log(document.querySelectorAll('.item'));
        /*similar as above, it's not a nodelist but a html collection
         the diff. is that with the 2nd we cant use array methods on,
         we have to manually convert to an array */
    console.log(document.getElementsByClassName('item'));
    console.log(document.getElementsByTagName('li'));

    const items = document.querySelectorAll('.item');
    items.forEach((item) => console.log(item));

    //changing things in DOM
        const ul = document.querySelector('.items');
        /*ul remove();
        ul.lastElementChild.remove();*/
        ul.firstElementChild.textContent = 'Hello';
        ul.children[1].innerText = 'Brad';
        ul.lastElementChild.innerHTML = '<h1>Hello</h1>';

        const btn = document.querySelector('.btn');
        btn.style.background = 'black';

    /*Events [after an event like click,mouseover,mouseout, should follow a function
    or an arrow function like in our case (e)=> ]
    prevent line is for submit btn when pushed to not go away
    listed at console are atributtes of the event(e),
    change of form color, add class, change text at event,etc.
    const butn = document.querySelector('.btn');
        butn.addEventListener('click', (e) => {
            e.preventDefault();
            document.querySelector('#my-form').style.background =
            '#ccc';
            document.querySelector('body').classList.add('bg-yellow');
            document.querySelector('.items').children[1].innerHTML =
            '<h3>Hello</h3>';
            console.log(e.target);
            console.log(e.target.className);   
        });
        add in comment the above due to below*/
    
    //Create an app - input user, take the value and add it in the list
    //Take stuff from DOM and input into var. as below
    const myform = document.querySelector('#my-form');
    const nameInput = document.querySelector('#name');
    const emailInput = document.querySelector('#email');
    const msg = document.querySelector('.msg');
    const userList = document.querySelector('#users');
    
    myform.addEventListener('submit', onSubmit);
    function onSubmit(e) {
        e.preventDefault();
        console.log(nameInput.value);
        
    if (nameInput.value === '' || emailInput.value === '') {
        msg.classList.add('error');  
        msg.innerHTML = 'Please enter all fields';
        setTimeout(() => msg.remove(),3000);
    } else {
        const li = document.createElement (li);
        li.appendChild(document.createTextNode
            (`${nameInput.value} : ${emailInput.value}`
        ));
        userList.appendChild(li);
        //Clear fields
        nameInput.value='';
        emailInput.value='';
        }
    }

    
  
    
    










